NSwag Generator

https://github.com/RicoSuter/NSwag/wiki/CSharpClientGeneratorSettings

https://github.com/RicoSuter/NSwag/wiki/CSharpClientGenerator

## Install nswag command
```sh
npm install nswag -g
```

## Generator Command
```sh
nswag swagger2csclient /Input:fareye.swagger.json /Classname:MyApiClient /GenerateBaseUrlProperty:false /ClientBaseClass:ApiClientBase /UseBaseUrl:true /ConfigurationClass:ApiConfig /InjectHttpClient:false /UseHttpClientCreationMethod:true /Namespace:MyProject.Api /Output:MyApiClient.cs /excludedTypeNames:HttpStatusCode /additionalNamespaceUsages:System.Net,MyProject.Model
```

## ApiClientBase 
```csharp
public partial class ApiClientBase
{
    protected TokenManager _tokenManager;
    public ApiClientBase(ApiConfig apiConfig)
    {
        _tokenManager = new TokenManager(apiConfig);
        BaseUrl = apiConfig.HostUrl;
    }

    public string BaseUrl { get; private set; }

    protected async Task<HttpClient> CreateHttpClientAsync(CancellationToken cancellationToken)
    {
        var httpClient = new HttpClient(new LoggingHandler(LogAction));
        var token = await _tokenManager.GetTokenAsync();
        httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token.AccessToken}");
        return httpClient;
    }

    public Func<Log, Task> LogAction;
}
```

## Token Generator
```csharp
public class TokenManager
{
    private ApiConfig _apiConfig;

    public TokenManager(ApiConfig apiConfig)
    {
        _apiConfig = apiConfig;
    }

    public Token GetToken(bool ignoreCache = false)
    {
        var token = (Token)MemoryCache.Default.Get("token");
        if (token != null && token.ExpiresAt.HasValue && DateTime.Now.AddMinutes(30) < token.ExpiresAt.Value)
            return token;

        var restCall = GetRestCall();
        var response = restCall.Client.Execute<Token>(restCall.Request);
        if (response.StatusCode != HttpStatusCode.OK)
            throw new Exception(response.ToJson());

        token = response.Data;
        token.ExpiresAt = DateTime.Now.AddSeconds(response.Data.ExpiresIn);

        MemoryCache.Default.Set("token", token, new CacheItemPolicy());

        return token;
    }

    public async Task<Token> GetTokenAsync(bool ignoreCache = false)
    {
        var token = (Token)MemoryCache.Default.Get("token");
        if (token != null && token.ExpiresAt.HasValue && DateTime.Now.AddMinutes(30) < token.ExpiresAt.Value)
            return token;

        var restCall = GetRestCall();
        var response = await restCall.Client.ExecuteAsync<Token>(restCall.Request);
        if (response.StatusCode != HttpStatusCode.OK)
            throw new Exception(response.ToJson());

        token = response.Data;
        token.ExpiresAt = DateTime.Now.AddSeconds(response.Data.ExpiresIn);

        MemoryCache.Default.Set("token", token, new CacheItemPolicy());

        return token;
    }

    private (RestRequest Request, RestClient Client) GetRestCall()
    {
        if (_apiConfig.HostUrl.Last() == '/')
            _apiConfig.HostUrl = _apiConfig.HostUrl.Remove(_apiConfig.HostUrl.Length - 1);

        var restClient = new RestClient($"{_apiConfig.HostUrl}/api/token");
        var restRequest = new RestRequest(Method.POST);
        restRequest.AddHeader("cache-control", "no-cache");
        restRequest.AddHeader("content-type", "application/x-www-form-urlencoded");
        restRequest.AddParameter("application/x-www-form-urlencoded", $"grant_type=password&username={_apiConfig.Username}&password={_apiConfig.Password}", ParameterType.RequestBody);

        return (restRequest, restClient);
    }
}
```

# Model Classes

```csharp
public class ApiConfig
{
    public string HostUrl { get; set; }
    public string ApiKey { get; set; }
}
```

```csharp
public class Token
{
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }
 
    [JsonProperty("token_type")]
    public string TokenType { get; set; }

    [JsonProperty("expires_in")]
    public int ExpiresIn { get; set; }

    public DateTime? ExpiresAt { get; set; }

    [JsonProperty("refresh_token")]
    public string RefreshToken { get; set; }
}
```