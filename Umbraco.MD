###Models Builder Configuration
- API Models : [Click here to get Instructions](/Development-Backend/Umbraco/Umbraco-Models-Builder)
  - Source: https://24days.in/umbraco-cms/2016/getting-started-with-modelsbuilder/

### Plugins (Nugets a instalar no Website.Web.Frontend)
- https://www.nuget.org/packages/DiploTraceLogViewer/ (Trace Log Viewer)
- https://www.nuget.org/packages/Tribe.UmbracoModuleThumbnails/1.0.0-CI-20180223-173320
- https://www.nuget.org/packages/Our.Umbraco.CmsEnvironmentIndicator/ (CMS Environment Indicator)
- https://www.nuget.org/packages/Cogworks.Meganav/ (Navigation menus Tool)
- https://www.nuget.org/packages/RankOne/ (Seo toolkit for Umbraco)
-https://our.umbraco.org/projects/developer-tools/diplo-god-mode/ (GOD MODE QUERY ALL)

- https://www.nuget.org/packages/Mailzory/  (Send emails based on Razor templates)




<br/><br/><hr/>
<IMG src="http://uiomatic.readthedocs.io/en/stable/img/uiomatic.png" alt="ui-o-matic logo"/><br/>

UI-O-Matic allows you to auto generate an integrated crud UI in Umbraco for a db table based on a petapoco (the default ORM in Umbraco) poco.
####Nuget :
  - https://www.nuget.org/packages/Nibble.Umbraco.UIOMatic/   (A instalar no projecto Core)
  - https://www.nuget.org/packages/Nibble.Umbraco.UIOMatic.Core/ (A instalar no projecto Frontend)
####Documentação
- http://uiomatic.readthedocs.io/en/stable/