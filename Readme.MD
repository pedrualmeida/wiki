# My Wiki

- **Host Web Application on Windows with IIS**
    - [.NET Framwork using commandline](ConfiguracaoIIS.MD)
    - [Host ASP.NET Core](HostDotNETCoreOnWindowsIIS.MD)
    - [Host Angular7 in IIS](HostAngularIoIIS.MD)
- [EntityFrameworkCodeFirst](EntityFrameworkCodeFirst.MD)
- [ReactJS](ReactJS.MD)
- [RoslynNetCompiler](RoslynNetCompiler.MD)
- [Swagger](Swagger.MD)
- [WebApiAutoLoggRequests](WebApiAutoLoggRequests.MD)
- [Webservices](Webservices.MD)
- [**Umbraco**](Umbraco.MD)
    - [APIJsonCamelCase](UmbracoAPIJsonCamelCase.MD)
    - [ModelsBuilder](UmbracoModelsBuilder.MD)
    - [TinyMceCustomization](UmbracoTinyMceCustomization.MD)
    - [TinyMceToolbar](UmbracoTinyMceToolbar.MD)
- [Javascript - LINQ equivalent Functions](JS-LINQ.MD)