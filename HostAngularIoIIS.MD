# IIS Settings: 
https://blog.angularindepth.com/deploy-an-angular-application-to-iis-60a0897742e7

### Requirements :
- URL Rewrite Module : https://www.iis.net/downloads/microsoft/url-rewrite

#### Add webconfig reference to `/angular.json`
Add `"src/web.config"` to :
```json
{
    "projects": {
        "Project.Name": {
            "architect": {
                "build": {
                    "options": {
                        "assets": [
                            "src/web.config"
                        ]
                    }
                }
            }
        }
    }
}
```
![image.png](https://i.imgur.com/2QLt0GT.png)

#### Add file `/src/web.config`:
```XML
<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <system.webServer>
        <rewrite>
            <rules>
                <rule name="Angular Routes" stopProcessing="true">
                    <match url=".*" />
                    <conditions logicalGrouping="MatchAll">
                        <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
                        <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="./index.html" />
                </rule>
            </rules>
        </rewrite>
    </system.webServer>
</configuration>
```