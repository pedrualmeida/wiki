#Setup : Webconfig
Em alternativa ao modelo API, neste cenário especifica-se a pasta onde ficam guardados os modelos de umbraco criados, da seguinte forma:
```XML
<add key="Umbraco.ModelsBuilder.Enable" value="true" />
<add key="Umbraco.ModelsBuilder.ModelsMode" value="AppData" />
<add key="Umbraco.ModelsBuilder.ModelsDirectory" value="~/../Bial.Ongentys.Core/Models/Umbraco" />
<add key="Umbraco.ModelsBuilder.ModelsNamespace" value="Bial.Ongentys.Core.Models.Umbraco" /> <!-- opcional: apenas para ficar corente com a pasta -->
<add key="Umbraco.ModelsBuilder.AcceptUnsafeModelsDirectory" value="true" />
```

#Gerar os modelos
Para gerar os modelos, será necessário ir ao Umbraco -> Developers -> Models Builder -> Generate Models

Não esquecer incluir as classes geradas no Visual Studio.