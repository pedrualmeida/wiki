Para fazer logging de todas as chamadas que sejam feitas aos endpoints WebAPI:

Criar a seguinte classe no projeto onde estiverem os endpoints:

```
using log4net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Whatever.Wherever
{
    public class LogRequestAndResponseHandler : DelegatingHandler
    {

        private static readonly ILog Logger = LogManager.GetLogger(typeof(LogRequestAndResponseHandler));

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            // log request body
            string requestBody = await request.Content.ReadAsStringAsync();
            Logger.DebugFormat("REQUEST: {0} ({1}) - {2}" , request.RequestUri.PathAndQuery, request.Method, requestBody);

            // let other handlers process the request
            var result = await base.SendAsync(request, cancellationToken);

            if (result.Content != null)
            {
                // once response body is ready, log it
                var responseBody = await result.Content.ReadAsStringAsync();
                Logger.DebugFormat("RESPONSE ({0}): {1}", result.StatusCode, responseBody);
            }
            else
            {
                Logger.DebugFormat("RESPONSE ({0})", result.StatusCode);
            }

            return result;
        }
    }
}
```

Na classe WebApiConfig, adicionar a seguinte linha:

`config.MessageHandlers.Add(new LogRequestAndResponseHandler());`

(neste caso o log4net já deverá estar configurado no projeto)

A partir de agora, todas as chamadas de entrada serão logadas!