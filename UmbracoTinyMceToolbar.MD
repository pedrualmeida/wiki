### tinyMceConfig.config : Add new comands to toolbar
![image.png](UmbracoTinyMceToolbar/1.png)
```XML
<tinymceConfig>
    <commands>
        ...
        <command>
            <umbracoAlias>Blockquote</umbracoAlias>
            <icon>images/editor/quote.gif</icon>
            <tinyMceCommand value="" userInterface="false" frontendCommand="blockquote">blockquote</tinyMceCommand>
            <priority>75</priority>
        </command>
    </commands>
</tinymceConfig>
```